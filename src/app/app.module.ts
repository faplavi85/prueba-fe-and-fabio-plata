import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TramitesServiciosComponent } from './pages/tramites-servicios/tramites-servicios.component';
import { InicioSesionComponent } from './pages/inicio-sesion/inicio-sesion.component';

@NgModule({
  declarations: [
    AppComponent,
    TramitesServiciosComponent,
    InicioSesionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
