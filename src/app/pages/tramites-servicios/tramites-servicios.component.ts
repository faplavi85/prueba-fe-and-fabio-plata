import { Component, OnInit, OnDestroy } from '@angular/core';

declare var require: any;
const datacuentanos: any[] = require('../../shared/data/cuentanos.json');
const dataotrostemas: any[] = require('../../shared/data/otrostemas.json');

@Component({
  selector: 'app-tramites-servicios',
  templateUrl: './tramites-servicios.component.html',
  styleUrls: ['./tramites-servicios.component.css']
})
export class TramitesServiciosComponent implements OnInit, OnDestroy {

  public listacuentanos: any = [];
  public listaotrostemas: any = [];

  constructor() {
    // Guarda JSON información en memoria
    localStorage.setItem('locallistacuentanos', JSON.stringify(datacuentanos));
    localStorage.setItem('locallistaotrostemas', JSON.stringify(dataotrostemas));
  }

  ngOnInit() {
    // Obtiene la información actualizada de memoria para mostrar en pantalla
    this.listacuentanos = JSON.parse(localStorage.getItem('locallistacuentanos'));
    this.listaotrostemas = JSON.parse(localStorage.getItem('locallistaotrostemas'));
  }

  ngOnDestroy() {
    // Líbera la memoria
    localStorage.removeItem('locallistacuentanos');
    localStorage.removeItem('locallistaotrostemas');
  }

}
