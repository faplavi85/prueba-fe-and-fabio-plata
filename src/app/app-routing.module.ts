import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TramitesServiciosComponent } from './pages/tramites-servicios/tramites-servicios.component';
import { InicioSesionComponent } from './pages/inicio-sesion/inicio-sesion.component';


const routes: Routes = [
  { path: '', component: TramitesServiciosComponent},
  { path: 'tramites-y-servicios', component: TramitesServiciosComponent },
  { path: 'ingreso', component: InicioSesionComponent },
  // otra url redirige a principal
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
